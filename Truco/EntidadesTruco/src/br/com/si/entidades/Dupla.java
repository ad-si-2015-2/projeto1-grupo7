/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.entidades;

import java.io.Serializable;

/**
 * Representa uma dupla no jogo. dupla1 ou dupla2 
 * @author Paulo Rogério Oliveira da Silva
 */
public enum Dupla implements Serializable {
    Dupla1, Dupla2
}
