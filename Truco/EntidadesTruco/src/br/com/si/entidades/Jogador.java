/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.entidades;

import java.io.Serializable;
import java.util.Objects;

/**
 * Classe com o objetivo de representar um jogador no jogo de truco
 * @author Paulo Rogério Oliveira da Silva
 */
public class Jogador implements Serializable {
    /*Propriedades*/
    private final String nome;
    private final String ip;
    

    /***
     * Controi um jogador.
     * @param nome do Jogador
     * @param ip da máquina 
     */
    public Jogador(String nome, String ip) {
        this.nome = nome;
        this.ip = ip;

    }    
    
    
    /***
     * 
     * @return Nome do jogador
     */
    public String getNome() {
        return nome;
    }

    /***
     * 
     * @return Ip da máquina do jogador
     */
    public String getIp() {
        return ip;
    }

    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.nome);
        hash = 29 * hash + Objects.hashCode(this.ip);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Jogador other = (Jogador) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        return Objects.equals(this.ip, other.ip);
    }
    
    @Override
    /***
     * @return string representando um jogador
     */
    public String toString(){
        return this.nome;
    }
    
            
    
    
}
