/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.entidades;

import java.io.Serializable;

/**
 * Enum que presenta o naipe de uma carta de baralho
 * @author Paulo Rogério Oliveira da Silva
 */
public enum Naipe implements Serializable {
    OURO {
        @Override
        public String toString() {
            return "O";
        }
    }, ESPADAS {
        @Override
        public String toString() {
            return "E";
        }
    }, COPAS {
        @Override
        public String toString() {
            return "C";
        }
    }, PAUS {
        @Override
        public String toString() {
            return "P";
        }
    }
     
}
