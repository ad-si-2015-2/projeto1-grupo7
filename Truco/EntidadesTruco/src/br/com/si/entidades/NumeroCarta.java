/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.entidades;

import java.io.Serializable;

/**
 * Enum que representa o número de uma carta de baralho
 * @author Paulo Rogério Oliveira da Silva
 */
public enum NumeroCarta implements Serializable {
    QUATRO {
        @Override
        public String toString() {
            return "4";
        }
    }, CINCO {
        @Override
        public String toString() {
            return "5";
        }
    }, SEIS {
        @Override
        public String toString() {
            return "6";
        }
    }, SETE {
        @Override
        public String toString() {
            return "7";
        }
    }, RAINHA {
        @Override
        public String toString() {
            return "Q";
        }
    }, VALETE {
        @Override
        public String toString() {
            return "J";
        }
    }, REI {
        @Override
        public String toString() {
            return "K";
        }
    }, AS {
        @Override
        public String toString() {
            return "A";
        }
    }, DOIS {
        @Override
        public String toString() {
            return "2";
        }
    }, TRES {
        @Override
        public String toString() {
            return "3";
        }
    }
}
