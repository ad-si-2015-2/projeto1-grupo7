/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.comunicacao;

import br.com.si.controlador.ControladorTruco;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Classe que representa um cliente remoto do servidor. Com o objetivo de obter
 * os dados da solicitação, passá-los ao controlador principal e enviar a
 * resposta ao cliente
 *
 * @author Paulo Rogério Oliveira da Silva
 */
public class RequisicaoRemota implements Runnable {

    private final Servidor servidor;
    private final String ipCliente;

    /**
     * *
     * Contrutor do cliente remoto
     *
     * @param servidor que contém os canais de comunicação com o cliente
     * @param ipCliente Ip da máquina solicitante.
     * @throws java.io.IOException
     */
    public RequisicaoRemota(Servidor servidor, String ipCliente) throws IOException {
        this.servidor = servidor;
        this.ipCliente = ipCliente;
        PrintStream p = new PrintStream(servidor.getCanalDeSaida(ipCliente));
        p.println("Digite .ajuda para informacoes sobre o jogo.");
    }

    /**
     * O cliente informa ao sokete um fluxo de caracteres que pode conter o caracter backSpace
     * Dessa formar torna-se necessário retirá-lo
     * @param string a qual se quer remover os caracteres especiais
     * @return string sem os caracteres especiais
     */
    private String removeBackSpace(String string) {
       
       ArrayList<Character> caracteresValidos = new ArrayList<>();       
       Character[] caracteresOriginais = new Character[string.length()];
       
       int i =0;
       for(char c: string.toCharArray()){
           caracteresOriginais[i++] = c;
       }
       caracteresValidos.addAll(Arrays.asList(caracteresOriginais));

       for( int k= 0, j = 0; k<string.length(); k++){
           if( caracteresOriginais[k] == '\b'  ){
               caracteresValidos.remove(k-j++);
               if( k-j+1 > 0 ){                    
                    caracteresValidos.remove(k-j++);
               }
           }
       }
       char[] chars = new char[caracteresValidos.size()];
       for(int z = 0; z<chars.length; z++ ){
           chars[z] = caracteresValidos.get(z);
       }
       
       return String.valueOf(chars);
    }

    /**
     * *
     * Executa o fluxo a cada solicitação do cliente
     */
    @Override
    public void run() {

        try {
            Scanner s = new Scanner(servidor.getCanalDeEntrada(ipCliente));
            while (s.hasNextLine()) {
                String comando = s.nextLine();
                comando = removeBackSpace(comando);
                ControladorTruco.obterInstancia().servico(ipCliente, comando);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}
