/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.controlador;

/**
 * Classe que gerencia os descartes
 *
 * @author paulo.rogerio
 */
public class GerenciadorTruco implements OuvinteServico {

    @Override
    public void servicoExecutado(String ip, String acao) {
        /*Se a ação foi um descarte e não acabou o jogo */
        if (acao.equals("descartar") && !ControladorTruco.obterInstancia().getJogo().isFimDeJogo()) {

            /*Se a mão está finalizada, então será iniciado uma nova mão*/
            if (ControladorTruco.obterInstancia().getJogo().getMaoAtual().maoFinalizada()) {
                ControladorTruco.obterInstancia().servico("gerenciador", ".novaMao");
                
            } else /*Se a mão está finalizada, então será iniciado uma nova mão*/ if (ControladorTruco.obterInstancia().getJogo().getMaoAtual().getRodadaAtual().isRodadaFinalizada() ) {
                ControladorTruco.obterInstancia().servico("gerenciador", ".novaRodada");
            }           
        } else if(acao.equals("correr") && !ControladorTruco.obterInstancia().getJogo().isFimDeJogo()){
             
            if (ControladorTruco.obterInstancia().getJogo().getMaoAtual().maoFinalizada()) {
                ControladorTruco.obterInstancia().servico("gerenciador", ".novaMao");
            }
        }
    }
}
