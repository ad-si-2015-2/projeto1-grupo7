/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.controlador;

/**
 * Classe observadora do controlador 
 * @author paulo.rogerio
 */
public interface OuvinteServico {
    
    /***
     * Notifica um ouvinte que um serviço foi executado
     * @param ip requisitante
     * @param acao executada
     */
    public void servicoExecutado(String ip, String acao);
}
