/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.execucao;

import br.com.si.comunicacao.RequisicaoRemota;
import br.com.si.comunicacao.Servidor;

/**
 *
 * @author Paulo Rogério Oliveira da Silva
 * 
 * Classe que inicia a aplicação servidora
 * 
 */
public class Principal {
    
   /***
    * Método que inicia a aplicação.
    * @param args parâmetros de inicialização
    * @throws java.io.IOException Erro de entrada e saída
    */ 
   public static void main(String...args) throws Exception{
       
       Servidor servidor = new Servidor();
       while(true){
           String ipCliente = servidor.receberConexao();
           new Thread(new RequisicaoRemota(servidor, ipCliente)).start();
       }
       
   }
    
    
}
