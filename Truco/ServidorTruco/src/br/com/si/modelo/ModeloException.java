/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.modelo;

/**
 *
 * @author paulo
 */
public class ModeloException extends RuntimeException {

    public ModeloException(String message) {
        super(message);
    }

    public ModeloException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
