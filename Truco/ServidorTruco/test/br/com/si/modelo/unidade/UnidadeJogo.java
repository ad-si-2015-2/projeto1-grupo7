/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.modelo.unidade;

import br.com.si.entidades.Carta;
import br.com.si.entidades.Dupla;
import br.com.si.entidades.Jogador;
import br.com.si.entidades.Vaga;
import br.com.si.modelo.Jogo;
import br.com.si.modelo.ModeloException;
import br.com.si.modelo.RodadaTruco;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;




/**
 * Objetivo da classe é fazer o fluxo perfeito ou seja. Nenhum erro deve 
 * ser lançado. Todas as classes do pacote pass seguem esse conceito.
 * @author Paulo Rogério Oliveira da Silva
 */
public class UnidadeJogo {
    private Jogo jogo; 
    private Jogador lider;
    private Carta carta;
    
    
    /***
     * Testa se o jogo pode ser criado e espera conexao dos jogadores
     */
    @Test
    //@Ignore
    public void criandoEsperandoConexao(){
        this.jogo = new Jogo("PicaFumo", new Jogador("Pedro","1"),4);
        assertNotNull("Jogo não criado",this.jogo);
        
    }
    
    /**
     * Adicionar todos os jogadores necessários para uma partida de truco
     * Ou seja adicionar mais tres jogadores
     */
    @Test
    public void adicionandoJogadores(){
        /*adicionando jogadores*/
        jogo.entrarNoJogo(new Jogador("Joao","2"));
        jogo.entrarNoJogo(new Jogador("Carlos","3"));
        jogo.entrarNoJogo(new Jogador("Rafel","4")); 
        assertNotNull(jogo.getVagas());
        jogo.getVagas().stream().forEach((v) -> {
            assertFalse("Há quatro jogadores, logo não deve haver vaga disponível", v.isDisponivel());
        });
          
    }
    
    
    
    /**
     * Iniciando o jogo
     */
    @Test    
    public void iniciandoJogo(){
        /*Conectando todos os jogadores*/
        jogo.entrarNoJogo(new Jogador("Joao","2"));
        jogo.entrarNoJogo(new Jogador("Carlos","3"));
        jogo.entrarNoJogo(new Jogador("Rafel","4")); 
        jogo.iniciar(lider);
      
    }
    
 
    
    /**
     * Método executado antes de cada teste
     */
    @Before
    public void configuraTeste() {
        this.jogo = new Jogo("PicaFumo", new Jogador("Chico", "1"),4);
        this.lider = jogo.getLider();
    }
    
    @Test
    /**
     * Testa se é possível realizar um descarte.
     */    
    public void descartando(){
        jogo.entrarNoJogo(new Jogador("Joao","2"));
        jogo.entrarNoJogo(new Jogador("Carlos","3"));
        jogo.entrarNoJogo(new Jogador("Rafel","4")); 
        
        jogo.iniciar(lider);
        RodadaTruco rodadaAtual = jogo.getMaoAtual().getRodadaAtual();
        carta = jogo.getMaoAtual().getCartasJogadores().get(lider).get(0);
        
        int quantidadeCartaMao = jogo.getMaoAtual().getCartasJogadores().get(lider).size();
        int quantidadeCartaDescartadas = rodadaAtual.getDescartesJogadores().size();
        
        rodadaAtual.descartar(lider, carta);
        assertTrue("A quantidade de cartas na mão do jogador deveria diminuir em 1", quantidadeCartaMao == jogo.getMaoAtual().getCartasJogadores().get(lider).size() +1 );        
        assertTrue("A quantidade de cartas descartadas deveria aumentar em 1", quantidadeCartaDescartadas == rodadaAtual.getDescartesJogadores().size()-1);
    }
    
    
    /**
     * Testando se ao tentar descartar sucessivamente será disparado a exceção
     */
    @Test(expected = ModeloException.class)
    public void descartandoSemPermissao(){
        jogo.entrarNoJogo(new Jogador("Joao","2"));
        jogo.entrarNoJogo(new Jogador("Carlos","3"));
        jogo.entrarNoJogo(new Jogador("Rafel","4")); 
        jogo.iniciar(lider);
        RodadaTruco rodadaAtual = jogo.getMaoAtual().getRodadaAtual();
        rodadaAtual.descartar(lider, jogo.getMaoAtual().getCartasJogadores().get(lider).get(0));
        rodadaAtual.descartar(lider, jogo.getMaoAtual().getCartasJogadores().get(lider).get(1));
    }
    
    /***
     * Testa se foi feito uma tentativa de descartar uma carta que não existe
     * na mão do usuário, ou seja está tentando descartar uma carta errada.
     */
    @Test(expected = ModeloException.class)
    public void descartandoCartaErrada(){
        jogo.entrarNoJogo(new Jogador("Joao","2"));
        jogo.entrarNoJogo(new Jogador("Carlos","3"));
        jogo.entrarNoJogo(new Jogador("Rafel","4")); 
        jogo.iniciar(lider);
        RodadaTruco rodadaAtual = jogo.getMaoAtual().getRodadaAtual();
        rodadaAtual.descartar(jogo.getVagas().get(3).getJogador(), jogo.getMaoAtual().getCartasJogadores().get(lider).get(0));
    }
    
    /***
     * Testando se é possível sair do jogo
     */
    @Test
    public void saindoDoJogo(){
        jogo.entrarNoJogo(new Jogador("Joao","2"));
        jogo.entrarNoJogo(new Jogador("Carlos","3"));
        jogo.entrarNoJogo(new Jogador("Rafel","4")); 
        
        jogo.iniciar(lider);
        Vaga vaga = jogo.getVagas().get(0);
        jogo.sairDoJogo(lider);
        assertTrue("Um jogador saiu logo deveria ter uma vaga disponível.", vaga.isDisponivel());

    }
    
    /***
     * Testando se todos os jogadores possuem duplas associadas.
     */
    @Test
    public void testaDuplasJogadores(){
        jogo.entrarNoJogo(new Jogador("Joao","2"));
        jogo.entrarNoJogo(new Jogador("Carlos","3"));
        jogo.entrarNoJogo(new Jogador("Rafel","4"));
        jogo.iniciar(lider);
        jogo.getJogadorDupla().entrySet().stream().forEach((e) -> {
            assertFalse("Todo jogador deveria ter uma dupla associada.",e.getValue() == null);
        });
        
    }
    
    
    @Test
    /**
     * Testa se a pontuação de mão da dupla aumenta
     */
    public void testePontuacao(){
       
        Jogador paulo = new Jogador("Paulo", "1");
        Jogador joao = new Jogador("Joao","2");
        Jogo j = new Jogo("1", paulo, 2);
        j.entrarNoJogo(joao);
        j.iniciar(paulo);
        

        /*Descartando até a mão acabar*/
        while( !j.getMaoAtual().maoFinalizada() ) {
            j.getMaoAtual().getRodadaAtual().descartar(paulo, j.getMaoAtual().getCartasJogadores().get(paulo).get(0));
            j.getMaoAtual().getRodadaAtual().descartar(joao, j.getMaoAtual().getCartasJogadores().get(joao).get(0));

            if (j.getMaoAtual().getRodadaAtual().isRodadaFinalizada()){
                j.getMaoAtual().novaRodada();
            }
        }
        Dupla vencedora = null;
        if( j.getMaoAtual().getDuplaVencedora().equals(Dupla.Dupla1)  ){
            assertTrue("A pontuação da dupla1 deveria ser 1", j.getPontuacaoDupla(Dupla.Dupla1) == 1);
            vencedora = Dupla.Dupla1;
        } else {
            vencedora = Dupla.Dupla2;
            assertTrue("A pontuação da dupla2 deveria ser 1", j.getPontuacaoDupla(Dupla.Dupla2) == 1);
        }
        j.novaMao();
        assertTrue("A pontuação da dupla deveria ser 1", j.getPontuacaoDupla(vencedora) == 1);
        
        
    }
    
    @Test
    /**
     * Testa se o jogo acaba
     */
    public void testeFinalizaJogo(){
       
        Jogador paulo = new Jogador("Paulo", "1");
        Jogador joao = new Jogador("Joao","2");
        Jogo j = new Jogo("1", paulo, 2);
        j.entrarNoJogo(joao);
        j.iniciar(paulo);
        

        /*Descartando até a mão acabar*/
        while( !j.isFimDeJogo()) {
            j.getMaoAtual().getRodadaAtual().descartar(paulo, j.getMaoAtual().getCartasJogadores().get(paulo).get(0));
            j.getMaoAtual().getRodadaAtual().descartar(joao, j.getMaoAtual().getCartasJogadores().get(joao).get(0));
 
            if(j.getMaoAtual().maoFinalizada()){
                j.novaMao();

            }
            if (j.getMaoAtual().getRodadaAtual().isRodadaFinalizada()){
                j.getMaoAtual().novaRodada();
            }
        }

        if( j.getDuplaVencedora().equals(Dupla.Dupla1)  ){
            assertTrue("A pontuação da dupla1 deveria ser 12", j.getPontuacaoDupla(Dupla.Dupla1) == 12);

        } else {
 
            assertTrue("A pontuação da dupla2 deveria ser 12", j.getPontuacaoDupla(Dupla.Dupla2) == 12);
        }
        
        
    }
    
    @Test
    /**
     * Testa se o jogo acaba quando um jogador sai do jogo
     */
    public void testeSairDoJogo(){
       
        Jogador paulo = new Jogador("Paulo", "1");
        Jogador joao = new Jogador("Joao","2");
        Jogo j = new Jogo("1", paulo, 2);
        j.entrarNoJogo(joao);
        j.iniciar(paulo);
        
        j.sairDoJogo(joao);

        assertTrue("O jogo deveria ter acabado", j.isFimDeJogo());
        assertTrue("A dupla vencedora deveria ser a 1", j.getDuplaVencedora().equals(Dupla.Dupla1) );
        
    }
    
}
