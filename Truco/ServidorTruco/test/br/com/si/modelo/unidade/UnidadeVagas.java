/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.modelo.unidade;

import br.com.si.entidades.Jogador;
import br.com.si.entidades.Vaga;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author paulo
 */
public class UnidadeVagas {
    

  
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void criandoUmaDisponivel() {
        Vaga v = new Vaga();
        Assert.assertTrue(v.isDisponivel());
    }
    
    @Test
    public void criandoVagaIndisponivel(){
        Vaga v = new Vaga();
        v.setJogador(new Jogador("Joao", "123"));
        Assert.assertFalse(v.isDisponivel());
    }
}
